const { MongoClient } = require('mongodb');
const assert = require('assert');

const url = "mongodb://localhost:27017";
const database = "wetaxidb";

const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });
var _db;

module.exports = {

  connection: function (callback) {
    try {
      client.connect(function (err) {
        assert.strictEqual(null, err);
        console.log('Connected successfully to MongoDB server on port 27017');

        _db = client.db(database);

        return callback(err, client);
      });

    } catch (e) {
      console.error(e);
    }
  },

  getDb: function () {
    return _db;
  },

  getWetaxiCollection: function () {
    return _db.collection('taxiCollection');
  },

  getAppCollection: function () {
    return _db.collection('appCollection');
  },

  getParkingCollection: function () {
    return _db.collection('parkingCollection');
  },

  closeConnection: function (callback) {
    client.close();
    return callback();
  },
};
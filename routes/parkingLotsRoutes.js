const express = require('express');
const router = express();
const { getParkingLots, updateParking, addParking, addVehicleInParking, removeVehicleInParking, getFirstVehicleOfParking } = require('../parking');



// GET 

router.get('/', (req, res) => {
  res.send('Parking Lots Root')
})

router.get('/parkingLots', async (req, res) => {
  const result = await getParkingLots();
  res.send(result)
})

router.get('/getFirstVehicleOfParking', async (req, res) => {
  const values = req.query; 
  const result = await getFirstVehicleOfParking(values);
  res.send(result)
})


// POST

router.post('/addParking', async (req, res) => {
  const values = req.body; //req.query 
  const result = await addParking(values);
  res.send(result);
})

router.post('/updateParking', (req, res) => {
  const values = req.body;
  const result = updateParking(values);
  res.send(result);
})

router.post('/addVehicleInParking', async (req, res) => {
  const values = req.body; //req.query 
  const result = await addVehicleInParking(values);
  res.send(result);
})

router.post('/removeVehicleInParking', async (req, res) => {
  const values = req.body; //req.query 
  const result = await removeVehicleInParking(values);
  res.send(result);
})



module.exports = router;
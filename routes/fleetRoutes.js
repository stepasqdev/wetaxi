const express = require('express');
const router = express();
const { getFleet, updateVehicle, addVehicle, updatePosition } = require('../fleet');



// GET 

router.get('/', (req, res) => {
  res.send('Fleet Root')
})

router.get('/fleet', async (req, res) => {
  const result = await getFleet();
  res.send(result)
})

router.get('/updateVehicle', async (req, res) => {
  const values = req.query;
  const result = await updateVehicle(values);
})

// POST

router.post('/addVehicle', async (req, res) => {
  const values = req.body; //req.query 
  const result = await addVehicle(values);
  res.send(result);
})

router.post('/updateVehicle', (req, res) => {
  const values = req.body;
  const result = updateVehicle(values);
  res.send(result);
})

router.post('/updatePosition', async (req, res) => {
  const values = req.body; //req.query 
  const result = await updatePosition(values);
  res.send(result);
})



module.exports = router;
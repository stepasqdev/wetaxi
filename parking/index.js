var dbConnect = require('../dbconnect');
const { ParkingModel } = require('../models/ParkingModel');
var moment = require('moment');
const { DateTime } = require('../models/MomentFormat');
var gpsFilter = require('gps-filter');
const { KalmanFilter } = require('kalman-filter');
const { ERROR_LABELS, LABELS } = require('../constants/Labels');
const { TAXI_STATUS } = require('../models/TaxiStatus');


const getParkingLots = async () => {
    const dbcollection = dbConnect.getParkingCollection();

    var cursor = dbcollection.find({});
    var parkingArray = await cursor.toArray();
    var parkingValues = parkingArray;
    return parkingValues;
}

const getFirstVehicleOfParking = async (parking) => {
    if (parking && parking.name) {
        const dbcollection = dbConnect.getParkingCollection();

        var cursor = dbcollection.findOne({ name: parking.name });
        var parking = await cursor;

        const vehicleList = parking.vehicleList;
        const firstInList = vehicleList.length > 0 ? vehicleList[0] : null;
        return resultMessage(firstInList);

    }
    return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA, { hint: 'name is required' }));

}

const addParking = async (parking) => {
    if (parking && parking.name) {
        let newParking = Object.assign({}, ParkingModel);

        newParking.name = parking.name;

        if (parking.boundaries) newParking.boundaries = parking.boundaries;

        newParking.lastUpdate = moment().format(DateTime);

        const dbcollection = dbConnect.getParkingCollection();
        dbcollection.insertOne(newParking)
        return resultMessage(LABELS.SUCCESS);

    }
    return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA, { hint: 'name is required' }));

}

const updateParking = async (parking) => {
    if (parking && parking.name) {

        const lastUpdate = moment().format(DateTime);
        const params = { lastUpdate: lastUpdate };

        if (parking.boundaries) params.boundaries = parking.boundaries;
        if (parking.available) params.available = parking.available;

        const dbcollection = dbConnect.getParkingCollection();
        const newValues = { $set: params };
        const query = { name: parking.name };

        dbcollection.updateOne(query, newValues)
        return resultMessage(LABELS.SUCCESS);

    }
    return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA, { hint: 'name is required' }));
}

const addVehicleInParking = async ({ parking, vehicle }) => {
    if (parking && parking.name && vehicle && vehicle.name) {
        const dbcollectionVehicle = dbConnect.getWetaxiCollection();
        var vehicleFound = dbcollectionVehicle.findOne({ name: vehicle.name });

        if (!vehicleFound) return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.VEHICLE_NOT_FOUND));

        const dbcollection = dbConnect.getParkingCollection();
        const parkingFound = await dbcollection.findOne({ name: parking.name });

        if (!parkingFound) return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.PARKING_NOT_FOUND));
        console.log(parkingFound)
        const vehicleFoundInParking = parkingFound.vehicleList.find((el) => el.name === vehicle.name);

        if (vehicleFoundInParking) return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.VEHICLE_ALREADY_IN_PARKING));

        const insertTime = moment().format(DateTime);

        dbcollection.updateOne({ name: parking.name }, { $push: { vehicleList: { name: vehicle.name, insertTime: insertTime } } })
        return resultMessage(LABELS.SUCCESS);

    }
    return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA, { hint: 'parking.name, vehicle.name are required' }));
}

const removeVehicleInParking = async ({ parking, vehicle }) => {
    if (parking && parking.name && vehicle && vehicle.name) {

        const dbcollection = dbConnect.getParkingCollection();
        const parkingFound = await dbcollection.findOne({ name: parking.name });

        if (!parkingFound) return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.PARKING_NOT_FOUND));

        const vehicleFoundInParking = parkingFound.vehicleList.find((el) => el.name === vehicle.name);

        if (!vehicleFoundInParking) return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.VEHICLE_NOT_FOUND));


        dbcollection.updateOne({ name: parking.name }, { $pull: { vehicleList: { name: vehicle.name } } })
        return resultMessage(LABELS.SUCCESS);

    }
    return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA, { hint: 'parking.name, vehicle.name are required' }));
}






const extraMessage = (message, extra) => {
    return { message: message, ...extra }
}


const resultMessage = (message, extra) => {
    return { result: message, ...extra }
}


module.exports = {
    getParkingLots,
    getFirstVehicleOfParking,
    addParking,
    updateParking,
    addVehicleInParking,
    removeVehicleInParking,
    extraMessage,
}

// db.taxiCollection.insert({ id: "T1", name: "taxi 1", car : "auto type 1", location : "", status : "", available : true, })
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config({ path: './.env' });
}
const express2 = require('express');
const routes = require('./routes/parkingLotsRoutes');

var dbConnect = require('./dbconnect');

dbConnect.connection(function (err, client) {
  if (err) console.log(err);


  const app = express2();

  const expresslayouts = require('express-ejs-layouts');

  app.set('view engine', 'ejs');
  app.set('views', __dirname + '/views');
  app.set('layout', 'layouts/layout');

  app.use(expresslayouts);
  app.use(express2.static('public'));
  app.use(express2.urlencoded({
    extended: false
  }));
  app.use(express2.json());

  app.use(routes);

  app.listen(process.env.PORT || 4000);
});


module.exports = express2;
#  GUIDE


## BEFORE STARTING THE PROJECT

### DATABASE
wetaxidb

### COLLECTIONS

taxiCollection, parkingCollection, appCollection


### IMPORT DATA

Import the following data into the MongoDb database:

parkingCollection:

`{ "_id" : ObjectId("60650bfa8e3f69a00a4d0875"), "name" : "Parking 1", "boundaries" : [ ], "vehicleList" : [ { "name" : "sample 7", "insertTime" : "2021-04-01 03:22:11" } ], "available" : true, "lastUpdate" : "2021-03-28 01:40:39" }
{ "_id" : ObjectId("6065165fbfee8f15505490f7"), "name" : "Parking 2", "boundaries" : [ ], "vehicleList" : [ ], "available" : true, "lastUpdate" : "2021-04-01 02:39:59" }`

taxiCollection:

`{"_id":"6063773e0ef0e6e929b16d83","name":"taxi 2","car":"auto type 1","location":"45.062726464544475, 7.679058191817662","status":"","available":true,"lastUpdate":null},
{"_id":"6063774c0ef0e6e929b16d84","name":"taxi 3","car":"auto type 2","location":"45.062726464544475, 7.679058191817662","status":"","available":true,"lastUpdate":null},
{"_id":"6063b0670ef0e6e929b16d85","name":"taxi 1","car":"auto type 1","location":"45.062726464544475, 7.679058191817662","status":"","available":true,"lastUpdate":null},
{"_id":"6063b9d1b7902641e4da39d6","name":"sample","car":"van type 1","location":"45.07665672196991, 7.668540613564359","status":"OFF","available":true,"lastUpdate":null},
{"_id":"6063ba09d5c79f2250ec1c7d","name":"sample 4","car":"van type 1","location":"45.07665672196991, 7.668540613564359","status":"OFF","available":true,"lastUpdate":null},
{"_id":"6063bac5445b1e08b402e939","name":"sample 6","car":"van type 1","location":"45.07966590294242, 20.665427464621840","status":"MOVING","available":true,"lastUpdate":"2021-04-01 01:52:38"},
{"_id":"6063bb2c18f20a1e98d6d0f8","name":"sample 7","car":"van type 1","location":"45.07665672196991, 7.668540613564359","status":"STATIONARY","available":true,"lastUpdate":"2021-03-31 02:25:53"}`



## GET STARTED

`npm i`

`npm run startDev` for the fleet section, main file `server.js`

`npm run startDevParking` for the parking section, main file `parkingLotsServer.js`

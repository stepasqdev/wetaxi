const ERROR_LABELS = {
    MISSING_DATA: 'MISSING_DATA',
    VEHICLE_NOT_FOUND: 'VEHICLE_NOT_FOUND',
    PARKING_NOT_FOUND: 'PARKING_NOT_FOUND',
    VEHICLE_ALREADY_IN_PARKING: 'VEHICLE_ALREADY_IN_PARKING',

}

const LABELS = {
    SUCCESS: 'success',
    ERROR: 'error',
    SKIPPED_GPS_UPDATE: 'SKIPPED_GPS_UPDATE', 
    UPDATED_GPS_VALUE: 'UPDATED_GPS_VALUE', 
}

module.exports = {
    ERROR_LABELS,
    LABELS,
}
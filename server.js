if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config({ path: './.env' });
}
const express = require('express');
const routes = require('./routes/fleetRoutes');

var dbConnect = require('./dbconnect');

dbConnect.connection(function (err, client) {
  if (err) console.log(err);


  const app = express();

  const expresslayouts = require('express-ejs-layouts');

  app.set('view engine', 'ejs');
  app.set('views', __dirname + '/views');
  app.set('layout', 'layouts/layout');

  app.use(expresslayouts);
  app.use(express.static('public'));
  app.use(express.urlencoded({
    extended: false
  }));
  app.use(express.json());

  app.use(routes);

  app.listen(process.env.PORT || 3000);
});

module.exports = express;

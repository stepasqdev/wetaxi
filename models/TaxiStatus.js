const TAXI_STATUS = {
    OFF: "OFF",
    ON: "ON",
    MOVING: "MOVING",
    STATIONARY: "STATIONARY",
}

module.exports = {
    TAXI_STATUS,
}
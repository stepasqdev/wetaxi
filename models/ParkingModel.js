
const ParkingModel = {
    // _id: null,
    name: null,
    boundaries: [],
    vehicleList: [],
    available: true,
    lastUpdate: null,
}

module.exports = {
    ParkingModel
}
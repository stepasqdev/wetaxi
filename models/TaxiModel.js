const { TAXI_STATUS } = require('../models/TaxiStatus');


const TaxiModel = {
    // _id: null,
    name: null,
    car: null,
    location: null,
    status: TAXI_STATUS.OFF,
    available: true,
    lastUpdate: null,
}

module.exports = {
    TaxiModel
}
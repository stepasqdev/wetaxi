var dbConnect = require('../dbconnect');
const { TaxiModel } = require('../models/TaxiModel');
var moment = require('moment');
const { DateTime } = require('../models/MomentFormat');
var gpsFilter = require('gps-filter');
const { KalmanFilter } = require('kalman-filter');
const { ERROR_LABELS, LABELS } = require('../constants/Labels');
const { TAXI_STATUS } = require('../models/TaxiStatus');
const { inside } = require('../utils/checkBoundaires');


const getFleet = async () => {
  const dbcollection = dbConnect.getWetaxiCollection();

  var cursor = dbcollection.find({});
  var fleetArray = await cursor.toArray();
  var fleetValues = fleetArray;

  return fleetValues;
}

const updateVehicle = async (vehicle) => {
  if (vehicle && vehicle.name) {
    const dbcollection = dbConnect.getWetaxiCollection();
    const vehicleName = vehicle.name;

    var cursor = dbcollection.find({ name: vehicleName });
    var fleetArray = await cursor.toArray();
    var fleetValues = fleetArray[0].fleet;
    return resultMessage(fleetValues);

  }
  return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA));
}

const addVehicle = async (vehicle) => {

  let newVehicle = Object.assign({}, TaxiModel);

  if (
    vehicle.name &&
    vehicle.car &&
    vehicle.location
  ) {

    newVehicle.name = vehicle.name;
    newVehicle.car = vehicle.car;
    newVehicle.location = vehicle.location;

    if (vehicle.status) newVehicle.status = vehicle.status;
    if (vehicle.available) newVehicle.available = vehicle.available;

    newVehicle.lastUpdate = moment().format(DateTime);

    const dbcollection = dbConnect.getWetaxiCollection();
    dbcollection.insertOne(newVehicle);
    // handle async response here
    return resultMessage(LABELS.SUCCESS);

  }

  return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA, { hint: 'name, location, car are required' }));
}

const splitGpsLocation = (location) => {
  return location.replace(/\s/g, '').split(',').map(val => parseFloat(val, 10));
}

// try gps noise reduction with Kalman filter
const filterGpsNoiseKalman = (newValue, oldValue) => {
  const splittedPointsNew = splitGpsLocation(newValue.location);
  const splittedPointsOld = splitGpsLocation(oldValue.location);
  const kFilter = new KalmanFilter({ observation: 2 });
  const observations = [splittedPointsNew, splittedPointsOld];

  const res = kFilter.filterAll(observations);
  return res;
}
// end try

const updatePosition = async (vehicle) => {

  if (vehicle.name && vehicle.location && vehicle.status) {

    const dbcollection = dbConnect.getWetaxiCollection();
    var cursor = dbcollection.findOne({ name: vehicle.name });
    var taxi = await cursor;
    const { status: oldStatus, location: oldLocation } = taxi;


    if (
      (vehicle.status === TAXI_STATUS.STATIONARY && oldStatus === TAXI_STATUS.STATIONARY) // if is not stationary and the value is already saved
      || (vehicle.location === oldLocation && vehicle.status === oldStatus)  // if the location is the same with the same status
    ) {
      // use the old value for avoid gps noise
      return resultMessage(LABELS.SUCCESS, extraMessage(LABELS.SKIPPED_GPS_UPDATE))
    } else {
      const lastUpdate = moment().format(DateTime);

      const newValues = { $set: { location: vehicle.location, status: vehicle.status, lastUpdate: lastUpdate } };
      const query = { name: vehicle.name };

      dbcollection.updateOne(query, newValues);

      //check if is in a parking to keep the position
      checkIfInParkingAndUpdate(vehicle);

      return resultMessage(LABELS.SUCCESS, extraMessage(LABELS.UPDATED_GPS_VALUE))
    }

  }
  return resultMessage(LABELS.ERROR, extraMessage(ERROR_LABELS.MISSING_DATA, { hint: 'name, location, status are required' }))

}

const checkIfInParkingAndUpdate = async (vehicle) => {
  const dbcollection = dbConnect.getParkingCollection();
  const parkingWithVehicleFound = await dbcollection.findOne({ vehicleList: { $elemMatch: { name: vehicle.name} } });

  if(!parkingWithVehicleFound) return;

  const parkingBoundaries= parkingWithVehicleFound.boundaries;
  const gpsBoundaries = parkingBoundaries.map(el => splitGpsLocation(el));
  const vehicleLocation = splitGpsLocation(vehicle.location);

  const isInParking = inside(vehicleLocation, gpsBoundaries);

  if(!isInParking) removeVehicleFromParking(vehicle, parkingWithVehicleFound.name);

}

const removeVehicleFromParking = (vehicle, parkingName) => {
  const dbcollection = dbConnect.getParkingCollection();
  dbcollection.updateOne({ name: parkingName }, { $pull: { vehicleList: { name: vehicle.name } } })
}

const extraMessage = (message, extra) => {
  return { message: message, ...extra }
}


const resultMessage = (message, extra) => {
  return { result: message, ...extra }
}


module.exports = {
  getFleet,
  updateVehicle,
  addVehicle,
  updatePosition,
  extraMessage,
}

// db.taxiCollection.insert({ id: "T1", name: "taxi 1", car : "auto type 1", location : "", status : "", available : true, })